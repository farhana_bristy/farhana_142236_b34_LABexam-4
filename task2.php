<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Get prime</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>

<!-- Button trigger modal -->
<button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
    Launch demo modal
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Carusel form</h4>
            </div>
            <div class="modal-body">





                <form action="getData.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="image">Image:</label>
                        <input type="file" class="form-control" name="image" placeholder="Add picture">
                    </div>
                    <div class="form-group">
                        <label for="text">Text:</label>
                        <textarea class="form-control" name="text" rows="4"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="date">Pick a date:</label>
                        <input type="date" name="date">
                    </div>


                    <button type="submit" class="btn btn-primary">Submit</button>


                </form>














            </div>

        </div>
    </div>
</div>

</body>
</html>